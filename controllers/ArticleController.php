<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers;


use app\extensions\Controller;
use general\ext\services\blog\BlogControllerTrait;

class ArticleController extends Controller {
	use BlogControllerTrait;
	public $defaultAction = 'list';
	public static $domain = 'diet-articles';
} 