<?php
use app\models\CategoryOfArticle;
use app\models\CategoryOfProduct;
use app\widgets\MaxWidthBlock;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="hdr-wrap">
	<div id="top-nav">
		<div id="logo">
			<a href="/">Мое сбалансированное питание</a>
			<div>Всё о питании и здоровье</div>
		</div>
		<div class="nav" id="top-menu">
			<?=
			Menu::widget([
				'items' => [
					[
						'label' => 'Статьи',
						'url' => ['article/list'],
					],
					[
						'label' => 'Продукты',
						'url' => ['product/index'],
					],
				]
			]);
			?>
			<!--ul>
				<li>
					<a title="" href="/fitness">Fitness</a>
					<ul>
						<li>
							<a title="" href="/fitness">Fitness</a>
						</li>
						<li>
							<a title="" href="/healthy-eating">Healthy Eating</a>
						</li>
						<li>
							<a title="" href="/weight-loss">Weight Loss</a>
						</li>
						<li>
							<a title="" href="/lifestyle">Lifestyle</a>
						</li>
						<li>
							<a title="" href="/celebrities">Celebrities</a>
						</li>
					</ul>
				</li>
				<li>
					<a title="" href="/healthy-eating">Healthy Eating</a>
				</li>
				<li>
					<a title="" href="/weight-loss">Weight Loss</a>
				</li>
				<li>
					<a title="" href="/lifestyle">Lifestyle</a>
				</li>
				<li>
					<a title="" href="/celebrities">Celebrities</a>
				</li>
			</ul-->
		</div>
		<div id="top-nav-btns">
            <input type="text" value="" class="search-btn" id="txt-search">
        </div>
	</div>
</div>
<div class="container">
	<div class="layout">
		<?
		MaxWidthBlock::begin([
			'maxWidth' => isset($this->params['width']) ? $this->params['width'] : null
		])
		?>
		<?=
		Menu::widget([
			'items' => [
				[
					'label' => 'Статьи',
					'template' => '<a class="side-name">{label}<span></span></a>',
					'active' => Yii::$app->controller->id == 'article',
					'items' => CategoryOfArticle::getMenu(),
				],
				[
					'label' => 'Продукты',
					'template' => '<a class="side-name">{label}<span></span></a>',
					'active' => Yii::$app->controller->id == 'product',
					'items' => CategoryOfProduct::getMenu(),
				],
			],
			'submenuTemplate' => '<ul>{items}</ul>',
			'activateParents' => true,
			'options' => [
				'class' => 'sidebar',
			],
		])
		?>
		<?= ''/*
		Menu::widget([
			'items' => [
				[
					'label' => 'Статьи',
					'template' => '<a class="side-name">{label}<span></span></a>',
					'active' => Yii::$app->controller->id == 'article',
					'items' => [
						[
							'label' => 'Главная',
							'url' => ['/'],
							'items' => [
								[
									'label' => 'Главная',
									'items' => [
										[
											'label' => 'Главная',
											'url' => ['article/list'],
										],
										[
											'label' => 'Все коктейли',
											'url' => ['article/index'],
										],
									],
								],
								[
									'label' => 'Все коктейли',
								],
							],
						],
						[
							'label' => 'Все коктейли',
						],
						[
							'label' => 'Коллекции',
						],
						[
							'label' => 'Бокалы',
						],
						[
							'label' => 'Компоненты',
						],
						[
							'label' => 'Фичи',
						],
					],
				],
				[
					'label' => 'Название',
					'template' => '<a class="side-name">{label}<span></span></a>',
					'items' => [
						[
							'label' => 'Главная',
						],
						[
							'label' => 'Все коктейли',
						],
						[
							'label' => 'Коллекции',
						],
						[
							'label' => 'Бокалы',
						],
						[
							'label' => 'Компоненты',
						],
						[
							'label' => 'Фичи',
						],
					],
				],
			],
			'submenuTemplate' => '<ul>{items}</ul>',
			'activateParents' => true,
			'options' => [
				'class' => 'sidebar',
			],
		])
		*/?>
		<div class="content">
			<div class="row">
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					'itemTemplate' => '<li>{link} » </li>',
				]) ?><!-- breadcrumbs -->
			</div>
			<div class="row">
				<h1><?= $this->title ?></h1>
			</div>
			<?= $content ?>
		</div>
		<?
		MaxWidthBlock::end();
		?>
	</div><!-- page -->
</div>
<div class="clear"></div>
<div class="footer nav">
	<div id="wrap-footer-menu">
		<div id="footer-menu">
			<div class="footer-column">
				<img src="/i/sl-logo.png" alt="Семь огней / Seven lights" width="150">
			</div>
			<div class="footer-column">
				<!--ul>
					<li>Главная</li>
					<li>Все коктейли</li>
					<li>Коллекции</li>
					<li>Бокалы</li>
					<li>Компоненты</li>
					<li>Фичи</li>
				</ul-->
			</div>
			<div class="footer-column">
				<!--ul>
					<li>Главная</li>
					<li>Все коктейли</li>
					<li>Коллекции</li>
					<li>Бокалы</li>
					<li>Компоненты</li>
					<li>Фичи</li>
				</ul-->
			</div>
			<div class="footer-column">
				<!--ul>
					<li>Главная</li>
					<li>Все коктейли</li>
					<li>Коллекции</li>
					<li>Бокалы</li>
					<li>Компоненты</li>
					<li>Фичи</li>
				</ul-->
			</div>
			<div class="footer-column">
				<ul>
					<li><img src="/i/seventeen-plus.png" alt="18+"></li>
					<li class="small">Материалы сайта предназначены для аудитории старше 18 лет</li>
					<li class="small">Информация на сайте представлена исключительно в справочных целях</li>
					<li class="small">Обязательно проконсультируйтесь с врачом</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="wrap-copyright" class="nav">
                <span id="copyright">
                    &copy; 2014 - <?= date('Y') ?> Семь огней / Seven lights<br>
                </span>
	</div>
</div><!-- footer -->
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter28948955 = new Ya.Metrika({id:28948955, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/28948955" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
