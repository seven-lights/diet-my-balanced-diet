<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use general\ext\Helper;
use yii\helpers\Html;

/* @var $model app\models\Product */
?>
<div class="row row-product">
	<a href="<?= Yii::$app->urlManager->createUrl(['product/view', 'id' => $model['id']]) ?>" class="h2"><?= Html::encode($model['name']) ?></a>
	<? if(isset($model::getImageLinks($model, 'small')['small'])): ?>
		<a href="<?= Yii::$app->urlManager->createUrl(['product/view', 'id' => $model['id']]) ?>" class="small_image_article">
			<img src="<?= $model::getImageLinks($model, 'small')['small'] ?>">
		</a>
	<? endif; ?>
	<?= Helper::cutter($model['description']) ?>
</div>