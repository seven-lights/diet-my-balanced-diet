<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$this->params['breadcrumbs'][] = $name;
?>
<div class="row">
    <p><?= nl2br(Html::encode($message)) ?></p>
    <p><a href="<?= Yii::$app->urlManager->createUrl('/') ?>">На главную</a></p>
</div>
