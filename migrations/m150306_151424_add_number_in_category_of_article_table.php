<?php

use app\models\CategoryOfArticle;
use yii\db\Schema;
use yii\db\Migration;

class m150306_151424_add_number_in_category_of_article_table extends Migration
{
    public function up()
    {
	    $this->addColumn(CategoryOfArticle::tableName(), 'number', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0 COMMENT "Порядковый номер"');
    }

    public function down()
    {
        echo "m150306_151424_add_number_in_category_of_article_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
