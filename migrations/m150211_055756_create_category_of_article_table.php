<?php

use yii\db\Schema;
use yii\db\Migration;

class m150211_055756_create_category_of_article_table extends Migration
{
    public function up()
    {
	    $this->createTable('category_of_article', [
		    'id' => Schema::TYPE_PK . ' COMMENT "ID"',
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL COMMENT "Категория"',
		    'parent_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL COMMENT "ID родителя"',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('parent_id_FK_category_of_article', 'category_of_article', 'parent_id', 'category_of_article', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150211_055756_create_category_of_article_table cannot be reverted.\n";

        return false;
    }
}
