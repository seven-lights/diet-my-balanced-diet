<?php

use yii\db\Schema;
use yii\db\Migration;

class m150313_155651_add_copy_from_in_product_table extends Migration
{
    public function up()
    {
	    $this->addColumn('product', 'copy_from', Schema::TYPE_INTEGER . ' COMMENT "Копия от"');
	    $this->addForeignKey('copy_from_FK_product', 'product', 'copy_from', 'product', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m150313_155651_add_copy_from_in_product_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
